package ar.com.matuu.neosa.utils;

import java.io.InputStream;
import java.io.OutputStream;
import android.util.Log;
import ar.com.matuu.neosa.model.Mensaje;

public class Associate {
	private final InputStream mmInStream;
	private final OutputStream mmOutStream;
	private String uuid;
	private ConnectedThread conn2;
	private String password;

	public Associate(InputStream mmIS, OutputStream mmOS, String uuid,
			ConnectedThread ct, String pass) {
		this.mmInStream = mmIS;
		this.mmOutStream = mmOS;
		this.uuid = uuid;
		this.conn2 = ct;
		this.password = pass;
		Mensaje.resetNum_Sec();

	}

	public String byte2String(byte[] buffer, int bytes) {
		String msg = "";
		for (int i = 0; i < bytes; i++) {
			msg += (char) buffer[i];
		}
		Log.i(ConnectThread.TAG, "Mensaje recibido: " + msg);
		return msg;
	}

	public int process(){
		boolean isSuccess = false;
		boolean error = false;
		byte[] buffer = new byte[1024];
		int bytes;

		
		/* Concateno el uuid del equipo con la 
		 * contraseña elegida, y le calculo su hash.
		 * Luego, envío el hash como identificador de esta asociación.
		 */
		
		try {
			/* Envío un 2 indicando que deseo asociar un dispositivo
			 */
			conn2.write("2".toString().getBytes());
			
			while (!isSuccess && !error) {
				buffer = new byte[1024];
				bytes = mmInStream.read(buffer);
				// Espero que el servidor me envíe la orden
				String str = byte2String(buffer, bytes);
				Mensaje msj = new Mensaje(str);
				Mensaje.incrNum_sec();
				if (msj.isValid()) {
					switch (msj.getCode()) {
					case Mensaje.INIT:
            			//Creamos mensaje de inicio
            			Mensaje mBegin = new Mensaje();
            			mBegin.set("Iniciando", Mensaje.OK);
            			conn2.write(mBegin.toString().getBytes());
            			Log.i(ConnectThread.TAG, "Send: "+mBegin.toString());
            			break;
					case Mensaje.UUID_PASS:
						// Calculamos el hash del concatenado del uuid y el password
						String dato = Cifrado.md5(uuid + password);
						Mensaje mUuid = new Mensaje();
						mUuid.set(dato, Mensaje.UUID_PASS);
						conn2.write(mUuid.toString().getBytes());
						Log.i(ConnectThread.TAG, "Send: " + mUuid.toString());
						break;
					case Mensaje.ASSOCIATED:
						Mensaje assoc = new Mensaje();
						assoc.set("Ok. Asociado.", Mensaje.ASSOCIATED);
						conn2.write(assoc.toString().getBytes());
						Log.i(ConnectThread.TAG, "Send: " + assoc.toString());
						isSuccess = true;
						break;
					case Mensaje.NOT_ASSOC:
						Log.e(ConnectThread.TAG, "No Asociado :-(");
						return Mensaje.MSG_NOT_ASSOC;
					case Mensaje.MSG_NOT_IS_AUTH:
						return Mensaje.MSG_NOT_IS_AUTH;
					case Mensaje.ERROR:
						Log.e(ConnectThread.TAG, "ERROR");
						error = true;
						break;
					case Mensaje.OK:
						Log.i(ConnectThread.TAG, "OK recibido. Seguimos --> "+ buffer.toString());
						isSuccess = true;
						break;
					}

				} else { // No valido o error. Enviar ERROR al server
					Mensaje mError = new Mensaje();
					mError.set("Error encontrado", Mensaje.ERROR);
					conn2.write(mError.toString().getBytes());
					Log.i(ConnectThread.TAG, "Send: " + mError.toString());
					error = true;
				}

			}
			if(error){
				return Mensaje.MSG_NOT_ASSOC;
			}
			
			
			return Mensaje.MSG_ASSOC_OK;
		} catch (Exception ex) {
			Log.e(ConnectThread.TAG, "" + ex.getMessage().toString());		
			return Mensaje.MSG_ERROR_CONN;
		}
	}

}


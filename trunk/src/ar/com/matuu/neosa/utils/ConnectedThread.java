package ar.com.matuu.neosa.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import ar.com.matuu.neosa.PreferencesActivity;
import ar.com.matuu.neosa.model.Mensaje;

public class ConnectedThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
    private final int action;
    private final String passwd;
    private int result = 0;
    
    public ConnectedThread(BluetoothSocket socket, int action, String passwd) {
    	Log.i(ConnectThread.TAG, "Manejando conexión...");
        mmSocket = socket;
        this.action = action;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
        this.passwd = passwd;
        //Se toman los streams de entrada y salida, se utilizan objetos temporales 
        //porque los miemboes son finales
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) { }
 
        mmInStream = tmpIn;
        mmOutStream = tmpOut;
    }
    
    
    
    public void run() {
    	String uuid = PreferencesActivity.uuid_android;
    	if(action == Mensaje.ASSOCIATE){
    		Associate assoc = new Associate(mmInStream, mmOutStream, uuid, this, passwd);
    		result = assoc.process();
    		
    	}else 
    		if (action == Mensaje.AUTHENTICATE){
	    	Authenticate auth = new Authenticate(mmInStream, mmOutStream, uuid, this, passwd);
	    	result= auth.process();
    	}
    	
    }
 
    /* Call this from the main activity to send data to the remote device */
    public void write(byte[] bytes) {
        try {
        	Log.i(ConnectThread.TAG, "Enviando datos...");
            mmOutStream.write(bytes);
        } catch (IOException e) { }
    }
 
    /* Call this from the main activity to shutdown the connection */
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) { }
    }
    
    public int getResultado(){
    	return result;
    }
}

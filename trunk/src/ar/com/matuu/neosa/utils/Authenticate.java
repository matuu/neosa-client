package ar.com.matuu.neosa.utils;

import java.io.InputStream;
import java.io.OutputStream;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;
import ar.com.matuu.neosa.model.Mensaje;

public class Authenticate {

	private final InputStream mmInStream;
	private final OutputStream mmOutStream;
	private String uuid;
	private ConnectedThread conn2;
	private String password;

	public Authenticate(InputStream mmIS, OutputStream mmOS, String uuid,
			ConnectedThread ct, String pass) {
		this.mmInStream = mmIS;
		this.mmOutStream = mmOS;
		this.uuid = uuid;
		this.conn2 = ct;
		this.password = pass;

	}

	public String byte2String(byte[] buffer, int bytes) {
		String msg = "";
		for (int i = 0; i < bytes; i++) {
			msg += (char) buffer[i];
		}
		Log.i(ConnectThread.TAG, "Mensaje recibido: " + msg);
		return msg;
	}

public int process() {
	boolean isSuccess = false;
	boolean error = false;
	byte[] buffer = new byte[1024];
	int bytes;
	// reseteamos el numero de secuencia de los mensajes.
	// 
	/*
	 * Envío un 1 indicando que deseo autenticar un dispositivo
	 */
	try {
		conn2.write("1".toString().getBytes());
	} catch (Exception ex) {
		Log.e(ConnectThread.TAG, "" + ex.getMessage().toString());
		isSuccess = false;
		error = true;
	}
	// Log.i(ConnectThread.TAG,"UUID:"+ uuid);
	while (!isSuccess && !error) {

		try {
			buffer = new byte[1024];
			bytes = mmInStream.read(buffer);
			String str = byte2String(buffer, bytes);
			Mensaje msj = new Mensaje(str);
			Mensaje.incrNum_sec();
			if (msj.isValid()) {
				Log.i(ConnectThread.TAG, "Recv: " + msj.getMsj());
				switch (msj.getCode()) {
				case Mensaje.INIT:
					// Creamos mensaje de inicio
					Mensaje mBegin = new Mensaje();
					mBegin.set("Iniciando", Mensaje.OK);
					conn2.write(mBegin.toString().getBytes());
					Log.i(ConnectThread.TAG, "Send: " + mBegin.toString());
					break;
				case Mensaje.UUID_PASS:
					// Cremos mensaje con el hash de el uuid + pass
					Mensaje mUuid = new Mensaje();
					mUuid.set(Cifrado.md5(uuid + password),	Mensaje.UUID_PASS);
					conn2.write(mUuid.toString().getBytes());
					Log.i(ConnectThread.TAG, "Send: " + mUuid.toString());
					break;
				case Mensaje.MAC_DEV:
					// Creamos mensaje con la mac del telefono
					Mensaje mMac = new Mensaje();
					mMac.set(BluetoothAdapter.getDefaultAdapter()
							.getAddress(), Mensaje.MAC_DEV);
					conn2.write(mMac.toString().getBytes());
					Log.i(ConnectThread.TAG, "Send: " + mMac.toString());
					break;
				case Mensaje.AUTHENTICATED:
					Mensaje mAuth = new Mensaje();
					mAuth.set("Gracias", Mensaje.AUTHENTICATED);
					conn2.write(mAuth.toString().getBytes());
					Log.i(ConnectThread.TAG, "Autenticado :-)");
					isSuccess = true;
					break;
				case Mensaje.MSG_NOTHING:
					return Mensaje.MSG_NOTHING;
				case Mensaje.MSG_NOT_IS_ASSOC:
					return Mensaje.MSG_NOT_IS_ASSOC;
				case Mensaje.NOT_AUTH:
					Log.e(ConnectThread.TAG, "No Autenticado :-(");
					error = true;
					break;
				case Mensaje.ERROR:
					Log.e(ConnectThread.TAG, "ERROR");
					error = true;
					break;
				case Mensaje.OK:
					Log.i(ConnectThread.TAG, "OK recibido. Seguimos --> "
							+ buffer.toString());
					break;
				}
			} else {
				// No valido o error. Enviar ERROR al server
				Mensaje mError = new Mensaje();
				mError.set("Error encontrado", Mensaje.ERROR);
				conn2.write(mError.toString().getBytes());
				Log.i(ConnectThread.TAG, "Send: " + mError.toString());
				error = true;
			}
			if (error) {
				return Mensaje.MSG_NOT_UNPROT;
			}

		} catch (Exception ex) {

			Log.e(ConnectThread.TAG, "" + ex.getMessage().toString());
			isSuccess = false;
			error = true;

		}
	}
	if (error) {
		return Mensaje.MSG_NOT_UNPROT;
	}
	return Mensaje.MSG_AUTH_OK;

}
}

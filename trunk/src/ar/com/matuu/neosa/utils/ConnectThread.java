package ar.com.matuu.neosa.utils;

import java.io.IOException;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import ar.com.matuu.neosa.NeosaClientActivity;
import ar.com.matuu.neosa.model.Mensaje;

public class ConnectThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    public static final String TAG = "Neosa";
	public static final boolean D = NeosaClientActivity.D;
	private BluetoothAdapter mBluetoothAdapter = null;
	ConnectedThread conn;
	private int action;
	private int result = 0;
	private String passwd;
	
	public ConnectThread(BluetoothDevice device, int action, String pass) {
		// Porque mmSocket es final, se usa un objeto temporal
		BluetoothSocket tmp = null;
		passwd = pass;
		this.action = action;
		mmDevice = device;
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		// Obtener un bluetoothsocket para conectar con el bluetoothDevice dado.
		try {
			tmp = device.createRfcommSocketToServiceRecord(NeosaClientActivity.MY_UUID);
			Log.i(TAG, "Se creó el socket.");
		} catch (IOException e) {
		}
		mmSocket = tmp;
	}

    public void run() {
        // Cancelar busqueda porque hará lenta la conexión.
        mBluetoothAdapter.cancelDiscovery();

        try {
            //Conecta el dispositivo al socket
        	//Esto bloqueará el hilo hasta que lo consiga o se produce una excepción
        	Log.i(TAG, "Intentando conectar.");
            mmSocket.connect();
        } catch (IOException connectException) {
        	Log.e(TAG, "IOConn:"+connectException.getMessage());
        	result = Mensaje.MSG_ERROR_CONN;
            // Imposible conectar. Cerrando el socket
            try {
                mmSocket.close();
                Log.i(TAG, "Desconectó.");
                
            } catch (IOException closeException) {
            	Log.e(TAG, "IOClose:"+connectException.getMessage());
            }
            return;
        }
        Log.i(TAG, "Conectado.");
    	conn = new ConnectedThread(mmSocket, action, passwd);
    	conn.start();
    	try {
			conn.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	setResultado(conn.getResultado());
    	
    }
    
    public void write(byte[] bytes) {
    
    	Log.i(ConnectThread.TAG, "Enviando datos...");
        conn.write(bytes);
        
    }

    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) { }
    }

    public int getResultado(){
    	return result;
    }
    public void setResultado(int r){
    	result = r;
    }
    
}

package ar.com.matuu.neosa.model;

public class Dispositivo {
	
	String nombre, mac, uuid, pass;
	
	public Dispositivo(){
		mac ="0";
	}
	
	
	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}


	/**
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}


	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Dispositivo(String name, String mac){
		this.nombre = name;
		this.mac = mac;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	
	@Override
	public String toString(){
		return nombre +" ("+mac+")";
	}
	

}

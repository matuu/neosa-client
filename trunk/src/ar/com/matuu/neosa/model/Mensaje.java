package ar.com.matuu.neosa.model;

import java.util.Hashtable;

import android.util.Log;
import ar.com.matuu.neosa.NeosaClientActivity;

public class Mensaje {
	/*Número de secuencia. Es una propiedad de clase. 
	 * Se incrementa al validar, y al crear un mensaje
	 * para el envio con el metodo set.*/
	private static int num_sec;
	private int num_msj;
	private String msj;
	private int code;
	
	/*De estado*/
	public static final int ERROR = -1;
	public static final int OK = 200;
	/*De solicitud*/
	public static final int INIT = 100;
	public static final int UUID_PASS = 1000;
	public static final int MAC_DEV = 2000;
	/*De resultado*/
	public static final int NOT_AUTH = 501;
	public static final int AUTHENTICATED = 502;
	public static final int NOT_ASSOC = 401;
	public static final int ASSOCIATED = 402;
	/*Acciones*/
	public static final int ASSOCIATE = 400;
	public static final int AUTHENTICATE = 500;
	/* Mensajes de resultado*/
	public static final int MSG_AUTH_OK = 10, MSG_ASSOC_OK = 11, MSG_ERROR_CONN=12, 
			MSG_NOT_UNPROT=13, MSG_NOT_ASSOC=14, MSG_NOT_IS_ASSOC=15, MSG_NOT_IS_AUTH=16, 
			MSG_NOTHING=17, MSG_ERROR=0;
	
	public static final Hashtable<Integer, String>  mensajesResultado= new Hashtable<Integer, String>();
	 
	public void setMensajesResultado(){
		mensajesResultado.put(MSG_AUTH_OK, "Servidor Neosa desprotegido.");
		mensajesResultado.put(MSG_ASSOC_OK, "Dispositivo asociado correctamente. Verifique en el servidor Neosa.");
		mensajesResultado.put(MSG_ERROR_CONN, "Error de conexión. Intente nuevamente.");
		mensajesResultado.put(MSG_NOT_UNPROT, "No se pudo desproteger el servidor Neosa.");
		mensajesResultado.put(MSG_NOT_ASSOC, "No se pudo asociar el dispositivo.");
		mensajesResultado.put(MSG_NOT_IS_ASSOC, "Se esperaba la operación de Asociación");
		mensajesResultado.put(MSG_NOT_IS_AUTH, "Se esperaba la operación de Autenticación");
		mensajesResultado.put(MSG_ERROR, "Error indeterminado.");
		mensajesResultado.put(MSG_NOTHING, "Neosa Server no está protegido, nada que hacer.");
	}
	public Mensaje() {
		setMensajesResultado();
		
	}
	public Mensaje(String str){
		setMensajesResultado();
		try {
		String[] memb = str.split(";");
		if(memb.length!=3){
			msj= "Cadena sin formato";
			code = -1;
		}else{
			num_msj = Integer.parseInt(memb[0]);
			msj = memb[1];
			code = Integer.parseInt(memb[2]);
		}
		}catch (NullPointerException e){
			msj = str;
			code = -1;
			
		}catch(NumberFormatException ex){
			msj = "No es un número";
			code = -1;
		}
	}
	
	public boolean isValid(){
		if(code == INIT || code == MSG_NOTHING || code == MSG_NOT_IS_ASSOC || code == MSG_NOT_IS_AUTH){
			num_sec=num_msj;
			return true;
		}else if(num_msj == num_sec){
			Log.i(NeosaClientActivity.TAG, "Valido: "+this.msj);
			return true;
		}
		Log.e(NeosaClientActivity.TAG, "No válido: "+this.msj);
		return false;
	}
	@Override
	public String toString(){
		return num_sec+";"+msj+";"+code;
	}
	
	public void set( String msj, int code){
		num_sec++;
		this.msj = msj;
		this.code = code;
	}
	
	/**
	 * @return the num_recv
	 */
	public int getNum_recv() {
		return num_msj;
	}
	/**
	 * @param num_recv the num_recv to set
	 */
	public void setNum_recv(int num_recv) {
		this.num_msj = num_recv;
	}
	/**
	 * @return the num_sec
	 */
	public static int getNum_sec() {
		return num_sec;
	}
	/**
	 * @param num_sec the num_sec to set
	 */
	public static void resetNum_Sec() {
		Mensaje.num_sec = 0;
	}
	public static void incrNum_sec(){
		Mensaje.num_sec++;
	}
	/**
	 * @return the msj
	 */
	public String getMsj() {
		return msj;
	}
	/**
	 * @param msj the msj to set
	 */
	public void setMsj(String msj) {
		this.msj = msj;
	}
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
	
}

package ar.com.matuu.neosa;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import ar.com.matuu.neosa.utils.Encriptado;



public class BluetoothService extends Service {
	/**
	 * Clase que extiende de Service. Es el servicio que se encarga de conectarse y comunicarse
	 * con el cliente en la PC. 
	 * 
	 * */
	private BluetoothAdapter btAdapter =null;
	private static final int REQUEST_ENABLE_BT = 3;
	private static final String TAG = "Neosa";
	private final IBinder mBinder = new MyBinder();
	private final Handler mHandler = null;
	public static final int MESSAGE_READ = 2;
	
	@Override
	public void onCreate() {
		super.onCreate();
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        
        if (btAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            return;
        }
        if (!btAdapter.isEnabled()) {
            return;
        }
        AcceptThread acceptConn = new AcceptThread();
        acceptConn.start();
        Log.i(TAG, "Service started.");
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "Service stopped.");
 
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
		
	}
	
	
	
	public class MyBinder extends Binder {
		BluetoothService getService() {
			return BluetoothService.this;
		}
	}
	
	

	private class AcceptThread extends Thread {
	    private final BluetoothServerSocket mmServerSocket;
	 
	    public AcceptThread() {
	        // Use a temporary object that is later assigned to mmServerSocket,
	        // because mmServerSocket is final
	        BluetoothServerSocket tmp = null;
	        try {
	            // MY_UUID is the app's UUID string, also used by the client code
	        	Log.i(TAG, "Creando socketServer.");
	            tmp = btAdapter.listenUsingRfcommWithServiceRecord("Neosa", NeosaClientActivity.MY_UUID);
	        } catch (IOException e) { }
	        mmServerSocket = tmp;
	    }
	 
	    @Override
		public void run() {
	        BluetoothSocket socket = null;
	        // Keep listening until exception occurs or a socket is returned
	        while (true) {
	            try {
	            	Log.i(TAG, "Escuchando...");
	                socket = mmServerSocket.accept();
	            } catch (IOException e) {
	                break;
	            }
	            // If a connection was accepted
	            if (socket != null) {
	                // Do work to manage the connection (in a separate thread)
	            	Log.i(TAG, "Conexión entrante. Manejar...");
	            	ConnectedThread conn = new ConnectedThread(socket);
	            	conn.start();
	                
	                try {
						mmServerSocket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                break;
	            }
	        }
	    }
	 
	    /** Will cancel the listening socket, and cause the thread to finish */
	    public void cancel() {
	        try {
	            mmServerSocket.close();
	        } catch (IOException e) { }
	    }
	}
	
	private class ConnectedThread extends Thread {
	    private final BluetoothSocket mmSocket;
	    private final InputStream mmInStream;
	    private final OutputStream mmOutStream;
	 
	    public ConnectedThread(BluetoothSocket socket) {
	    	Log.i(TAG, "Manejando conexión...");
	        mmSocket = socket;
	        InputStream tmpIn = null;
	        OutputStream tmpOut = null;
	 
	        // Get the input and output streams, using temp objects because
	        // member streams are final
	        try {
	            tmpIn = socket.getInputStream();
	            tmpOut = socket.getOutputStream();
	        } catch (IOException e) { }
	 
	        mmInStream = tmpIn;
	        mmOutStream = tmpOut;
	    }
	 
	    @Override
		public void run() {
	        byte[] buffer = new byte[1024];  // buffer store for the stream
	        int bytes; // bytes returned from read()
	       
	        Log.i(TAG, "Esperando datos...");
	        // Keep listening to the InputStream until an exception occurs
	        while (true) {
	            try {
	                // Read from the InputStream
	                bytes = mmInStream.read(buffer);
	                String msg = "";
	                for(int i=0;i< bytes;i++){
	                	msg+=(char)buffer[i];
	                }
	                Log.e(TAG, msg);
	                
	                byte[] writeBuf =  Encriptado.getStringMessageDigest(msg, "MD5").getBytes();
	                
	                write(writeBuf);
	                
	            } catch (Exception e) {
	            	
	            	Log.e(TAG, ""+ e.getMessage().toString());
	            	cancel();
	                break;
	            }
	        }
	    }
	 
	    /* Call this from the main activity to send data to the remote device */
	    public void write(byte[] bytes) {
	        try {
	        	Log.i(TAG, "Enviando datos...");
	            mmOutStream.write(bytes);
	        } catch (IOException e) { }
	    }
	 
	    /* Call this from the main activity to shutdown the connection */
	    public void cancel() {
	        try {
	            mmSocket.close();
	        } catch (IOException e) { }
	    }
	}
	
	
	
} //FIN CLASE















///CLIENTE
/*
private class ConnectedThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
 
    public ConnectedThread(BluetoothSocket socket) {
        mmSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
 
        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) { }
 
        mmInStream = tmpIn;
        mmOutStream = tmpOut;
    }
 
    public void run() {
        byte[] buffer = new byte[1024];  // buffer store for the stream
        int bytes; // bytes returned from read()
 
        // Keep listening to the InputStream until an exception occurs
        while (true) {
            try {
                // Read from the InputStream
                bytes = mmInStream.read(buffer);
                // Send the obtained bytes to the UI activity
                mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                        .sendToTarget();
            } catch (IOException e) {
                break;
            }
        }
    }
 */
    /* Call this from the main activity to send data to the remote device */
  /*  public void write(byte[] bytes) {
        try {
            mmOutStream.write(bytes);
        } catch (IOException e) { }
    }
 */
    /* Call this from the main activity to shutdown the connection */
 /*   public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) { }
    }
}*/
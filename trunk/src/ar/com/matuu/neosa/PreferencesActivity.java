package ar.com.matuu.neosa;

import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import ar.com.matuu.neosa.model.Dispositivo;
import ar.com.matuu.neosa.model.Mensaje;
import ar.com.matuu.neosa.utils.ConnectThread;

@SuppressLint("DefaultLocale")
public class PreferencesActivity extends Activity {
	public static final String TAG = "Neosa";
	public static final boolean D = NeosaClientActivity.D;
	// Adaptador local BT
	private BluetoothAdapter mBluetoothAdapter = null;
	// Creo el UUID del servicio
	static final UUID MY_UUID = UUID
			.fromString("00001111-0000-1000-1000-121110987654");
	// Array de dispositivos
	private final ArrayList<Dispositivo> mArrayAdapter = new ArrayList<Dispositivo>();
	/// Dispositivo elegido como server
	public static Dispositivo dispServer = new Dispositivo();
	public static String uuid_android = "";
	private static final int SAVE_PREFERENCES = 2;
	private static final int REGISTER_DEV = 3;
	private SharedPreferences sp;
	private ProgressDialog dialog = null;
	private ConnectThread connSocket;
	
	Button scanDispositivosBoton;
	Button cleanConfig;
	TextView deviceConfigured;
	Button registrarDev;
	private ProgressDialog pDialog;
	private AsociarDispositivo asociarTarea;
	
	
	private String msjResultado = "";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (D) Log.e(TAG, "+++ ON CREATE Pref +++");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preferences);
		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		// Si el adaptador es null, el bluetooth no esta presente
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "No se encontró adaptador Bluetooth.",
					Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
		/* Configura la aplicación para que no se 
		 * estableca la orientación Lanpdscape
		 */
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
		    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		//busco las preferencias guardadas
		sp = getSharedPreferences("neosaPreferences", Context.MODE_PRIVATE);

		// filtros para atender a los eventos del bluetooth
		IntentFilter filterFoundDevice = new IntentFilter(
				BluetoothDevice.ACTION_FOUND);
		IntentFilter filterStart = new IntentFilter(
				BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		IntentFilter filterStop = new IntentFilter(
				BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		registerReceiver(mReceiver, filterFoundDevice);
		registerReceiver(mReceiver, filterStart);
		registerReceiver(mReceiver, filterStop);
		
		scanDispositivosBoton = (Button) findViewById(R.id.scan_devices_button);
		cleanConfig = (Button)findViewById(R.id.clean_config);
		deviceConfigured = (TextView) findViewById(R.id.device_server);
		registrarDev = (Button) findViewById(R.id.registar_dev);
		
		
		//acción del botón
		scanDispositivosBoton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mBluetoothAdapter.startDiscovery();
			}
		});
		
		//accion del boton
		cleanConfig.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SharedPreferences.Editor editor = sp.edit();
				editor.clear();
				if(editor.commit()) {
					deviceConfigured.setText("");
					cleanConfig.setEnabled(false);
					registrarDev.setEnabled(false);
					dispServer = null;
				}
			}
		});
		
		//acción del botón
		registrarDev.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v){
				((Activity) v.getContext()).showDialog(REGISTER_DEV);
			}
		});
		
		/* Muestro la configuración según 
		 * las preferencias guardadas.
		 */
		if (sp == null || sp.getString("neosaServerMac", "").equals("")) {
			deviceConfigured.setText("");
			cleanConfig.setEnabled(false);
			registrarDev.setEnabled(false);
			
		}else{
			dispServer.setMac(sp.getString("neosaServerMac", ""));
			dispServer.setNombre(sp.getString("neosaServerName", ""));
			dispServer.setPass(sp.getString("passwdDevice",""));
			deviceConfigured.setText(dispServer.toString());
			cleanConfig.setEnabled(true);
			if ( sp.getString("passwdDevice", "").equals("")) {
				registrarDev.setEnabled(true);
			}else{
				registrarDev.setEnabled(false);
			}
			
		}
		
		
		
		
		//Mostrar dispositivo emparejados previamente
		findPaired();
	}

	@Override
	public synchronized void onPause() {
		super.onPause();
		if (D)
			Log.e(TAG, "- ON PAUSE pref -");
	}

	@Override
	public void onStop() {
		super.onStop();
		mBluetoothAdapter.cancelDiscovery();
		if (D)
			Log.e(TAG, "-- ON STOP pref --");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mReceiver);
		if (D)
			Log.e(TAG, "--- ON DESTROY pref ---");
	}

	private void findPaired() {
		if (!mArrayAdapter.isEmpty())
			mArrayAdapter.clear();
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
		if (pairedDevices.size() > 0) {
			for (BluetoothDevice device : pairedDevices) {
				Dispositivo aux = new Dispositivo(device.getName(), device.getAddress());
				mArrayAdapter.add(aux);
			}
			ListView listaDeDispositivos = (ListView) findViewById(R.id.list_paired);
			DeviceAdapter adaptador = new DeviceAdapter(PreferencesActivity.this, mArrayAdapter);
			listaDeDispositivos.setAdapter(adaptador);
		}
	}

	public static Dispositivo getDispositivo() {
		return dispServer;
	}

	public static void setDispositivo(Dispositivo disp) {
		dispServer = disp;
	}

	private void savePreferences() {

		SharedPreferences.Editor editor = sp.edit();
		editor.putString("neosaServerName", dispServer.getNombre());
		editor.putString("neosaServerMac", dispServer.getMac());
		editor.commit();
		deviceConfigured.setText(dispServer.toString());
		cleanConfig.setEnabled(true);
		registrarDev.setEnabled(true);
		Toast info = Toast.makeText(this, "Dispositivo " +
						dispServer.toString()+" asociado. " +
						"Registre este dispositivo en Neosa Server.", Toast.LENGTH_LONG);
		
		info.show();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialogo = null;
		switch (id) {
		case SAVE_PREFERENCES:
			dialogo = crearDialogoAsociarDev();
			break;
		case REGISTER_DEV:
			dialogo = crearDialogoRegistrarDev();
		default:
			dialogo = null;
			break;
		}
		return dialogo;
	}

	private Dialog crearDialogoAsociarDev() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("¿Está seguro?");
		builder.setMessage("Se establecerá el dispositivo "
				+ dispServer.toString()+" como Neosa Server.");
		builder.setPositiveButton(R.string.opt_aceptar,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (D) Log.i("Dialogos", "Confirmacion Aceptada.");
						savePreferences();
						
					}
				});
		builder.setNegativeButton(R.string.opt_cancelar,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (D) Log.i("Dialogos", "Confirmacion Cancelada.");
						dialog.cancel();
					}
				});
		return builder.create();
	}
	
	private Dialog crearDialogoRegistrarDev() {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Registrando dispositivo");
		alert.setMessage("Ingrese la clave elegida para la autenticación:");

		final EditText input = new EditText(this);
		
		input.setTransformationMethod(PasswordTransformationMethod.getInstance());
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
			dispServer.setPass(input.getText().toString());
		  	pDialog = new ProgressDialog(PreferencesActivity.this);
	        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	        pDialog.setMessage("Asociando dispositivo...");
	        pDialog.setCancelable(true);
	        pDialog.setMax(100);
	 
	        asociarTarea = new AsociarDispositivo();
	        asociarTarea.execute();
		  }
		});

		alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		return alert.show();
		
	}
	
	
	private class AsociarDispositivo extends AsyncTask<Void, Integer, Boolean> {
		 
        @Override
        protected Boolean doInBackground(Void... params) {
 
        	int r = startConnection(Mensaje.ASSOCIATE, dispServer.getPass());
        	
        	if(r == Mensaje.MSG_ASSOC_OK){
        		SharedPreferences.Editor editor = sp.edit();
      			editor.putString("passwdDevice", dispServer.getPass());
      			editor.commit();
        	}
        	
        		
  			msjResultado = Mensaje.mensajesResultado.get(r);    			
        	
            return true;
        }
 
        
 
        @Override
        protected void onPreExecute() {
 
            pDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            	AsociarDispositivo.this.cancel(true);
            }
        });
 
            pDialog.setProgress(0);
            pDialog.show();
        }
 
        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                pDialog.dismiss();
                if(!sp.getString("passwdDevice","").equals("")){
                	registrarDev.setEnabled(false);
                }
                Toast.makeText(PreferencesActivity.this, msjResultado,
                    Toast.LENGTH_LONG).show();
            }
        }
 
        
    }
	
	public int startConnection(int tipo, String pass) {

		if (connSocket != null) {
			connSocket.cancel();
		}
		Log.i(TAG, "Iniciando conexión.");

		Dispositivo disp = PreferencesActivity.dispServer;
		if (disp.getMac() != "") {
			BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(disp.getMac().toUpperCase());

			connSocket = new ConnectThread(device, tipo, pass);
			connSocket.start();
			try {
				connSocket.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return connSocket.getResultado();

		}
		return 0;

	}
	

	// Create a BroadcastReceiver for ACTION_FOUND
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		/**
		 * Clase que implementa acciones a eventos registrados.
		 * 
		 */
		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();
			Log.d(TAG, action);
			ListView listaDeDispositivos = (ListView) findViewById(R.id.list_paired);
			// Se encontró un dispositivo
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				//Se toma el objeto bluetooth desde el intent
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				// Se añade al array
				Dispositivo aux = new Dispositivo(device.getName(),	device.getAddress());
				mArrayAdapter.add(aux);
				Log.d(TAG, aux.toString());
			}
			if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
				Log.d(TAG, "Started discovery");
				mArrayAdapter.clear();
				DeviceAdapter adaptador = new DeviceAdapter(
						PreferencesActivity.this, mArrayAdapter);
				listaDeDispositivos.setAdapter(adaptador);
				dialog = ProgressDialog.show(context, "", "Buscando...", true);
			}
			if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
				Log.d(TAG, "Finished discovery");
				
				DeviceAdapter adaptador = new DeviceAdapter(
						PreferencesActivity.this, mArrayAdapter);
				listaDeDispositivos.setAdapter(adaptador);
				if(dialog != null || dialog.isShowing()) dialog.cancel();
			}
		}
	};
}

package ar.com.matuu.neosa;

import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import ar.com.matuu.neosa.model.Dispositivo;
import ar.com.matuu.neosa.model.Mensaje;
import ar.com.matuu.neosa.utils.ConnectThread;

@SuppressLint("DefaultLocale")
public class NeosaClientActivity extends Activity {
	/** Called when the activity is first created. */
	// Debugging
	public static final String TAG = "Neosa";
	public static final boolean D = true;
	// Constantes para los dialogos
	private static final int OPEN_PREFERENCES = 1;
	// Codigo de solicitud del Intent
	private static final int REQUEST_ENABLE_BT = 3;
	// Adaptador local BT
	private BluetoothAdapter mBluetoothAdapter = null;
	// Creo el UUID del servicio
	public static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	private ConnectThread connSocket;
	// public static Dispositivo dispServ= null;
	Button startTask;
	Button preferencesOpen;
	private ProgressDialog pDialog;
	private AutenticarDispositivo autenticarTarea;

	private String msjResultado = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (D)
			Log.e(TAG, "+++ ON CREATE +++");
		super.onCreate(savedInstanceState);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		setContentView(R.layout.main);
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		// Si el adaptador es null, el bluetooth no esta presente
		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "No se encontró adaptador Bluetooth.",
					Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
		PreferencesActivity.uuid_android = Secure.getString(
				this.getContentResolver(), Secure.ANDROID_ID);
		Log.i(TAG, PreferencesActivity.uuid_android);
		// Si no se configuró la mac y nombre del disositivo server
		SharedPreferences sp = getSharedPreferences("neosaPreferences",
				Context.MODE_PRIVATE);
		if (sp == null || sp.getString("neosaServerMac", "").equals("")) {
			showDialog(OPEN_PREFERENCES);
			PreferencesActivity.dispServer = null;
		} else {
			PreferencesActivity.dispServer.setMac(sp.getString(
					"neosaServerMac", ""));
			PreferencesActivity.dispServer.setNombre(sp.getString(
					"neosaServerName", ""));

		}

		// Tomando los componentes de la vista
		startTask = (Button) findViewById(R.id.start_task);
		preferencesOpen = (Button) findViewById(R.id.btn_preferencias);
		startTask.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (D)
					Log.e(TAG, "Boton iniciar servicio...");
				pDialog = new ProgressDialog(NeosaClientActivity.this);
		        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		        pDialog.setMessage("Desprotegiendo dispositivo...");
		        pDialog.setCancelable(true);
		        pDialog.setMax(100);
		 
		        autenticarTarea = new AutenticarDispositivo();
		        autenticarTarea.execute();
			}
		});
		preferencesOpen.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (D)
					Log.e(TAG, "Boton abrir preferencias...");
				openPreferences();
			}
		});
		if (PreferencesActivity.dispServer == null) {
			startTask.setEnabled(false);
		} else {
			startTask.setEnabled(true);
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		if (D)
			Log.e(TAG, "++ ON START ++");

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		}

	}

	@Override
	public synchronized void onResume() {
		super.onResume();
		if (D)
			Log.e(TAG, "+ ON RESUME +");
		if (PreferencesActivity.dispServer == null) {
			startTask.setEnabled(false);
		} else {
			startTask.setEnabled(true);
		}

	}

	@Override
	public synchronized void onPause() {
		super.onPause();
		if (D)
			Log.e(TAG, "- ON PAUSE -");
	}

	@Override
	public void onStop() {
		super.onStop();
		mBluetoothAdapter.cancelDiscovery();
		if (D)
			Log.e(TAG, "-- ON STOP --");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (D)
			Log.e(TAG, "--- ON DESTROY ---");
	}

	/************************************/
	/* METODOS FUNCIONALES */
	/************************************/
	private void openPreferences() {
		Intent i = new Intent(NeosaClientActivity.this,
				PreferencesActivity.class);
		startActivity(i);
	}

	public BluetoothAdapter getAdapter() {
		/**
		 * Retorna la intancia del adaptador BT local.
		 * 
		 * @return BluetoothAdapter
		 */
		return mBluetoothAdapter;
	}

public int startConnection(int tipo) {

	if (connSocket != null) {
		connSocket.cancel();
	}
	Log.i(TAG, "Iniciando conexión.");

	Dispositivo disp = PreferencesActivity.dispServer;
	if (disp.getMac() != "") {
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(disp
				.getMac().toUpperCase());
		SharedPreferences sp = getSharedPreferences("neosaPreferences",
				Context.MODE_PRIVATE);

		connSocket = new ConnectThread(device, tipo, sp.getString(
				"passwdDevice", ""));
		connSocket.start();

		try {
			connSocket.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	return connSocket.getResultado();
}

	/************************************/
	/* OPCIONES DEL MENU */
	/************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.MnuPreferences:
			openPreferences();
			return true;
		case R.id.MnuExit:
			Toast.makeText(this, "¡Buena Suerte!", Toast.LENGTH_LONG).show();
			NeosaClientActivity.this.finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/************************************/
	/* DIALOGOS y ALERTAS */
	/************************************/
	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialogo = null;
		switch (id) {
		case OPEN_PREFERENCES:
			dialogo = crearDialogo1();
			break;
		// ...
		default:
			dialogo = null;
			break;
		}
		return dialogo;
	}

	private Dialog crearDialogo1() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("¿Configuramos?");
		builder.setMessage("Debemos configurar Neosa antes de poder utilizarlo. ¿Desea hacerlo ahora?");
		builder.setPositiveButton(R.string.opt_aceptar,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (D)
							Log.i("Dialogos", "Confirmacion Aceptada.");
						openPreferences();
					}
				});
		builder.setNegativeButton(R.string.opt_cancelar,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (D)
							Log.i("Dialogos", "Confirmacion Cancelada.");
						dialog.cancel();
					}

				});
		return builder.create();
	}

	private class AutenticarDispositivo extends AsyncTask<Void, Integer, Boolean> {
		 
        @Override
        protected Boolean doInBackground(Void... params) {
 
        	
        	int r = startConnection(Mensaje.AUTHENTICATE);
        	msjResultado = Mensaje.mensajesResultado.get(r);
            return true;
        }
 
        
 
        @Override
        protected void onPreExecute() {
 
            pDialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            	AutenticarDispositivo.this.cancel(true);
            }
        });
 
            pDialog.setProgress(0);
            pDialog.show();
        }
 
        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                pDialog.dismiss();
                Toast.makeText(NeosaClientActivity.this, msjResultado,
                    Toast.LENGTH_LONG).show();
            }
        }
 
        
    }
}


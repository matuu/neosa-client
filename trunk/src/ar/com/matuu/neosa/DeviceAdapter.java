package ar.com.matuu.neosa;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ar.com.matuu.neosa.model.Dispositivo;

public class DeviceAdapter extends ArrayAdapter<Dispositivo> {
	// Debugging
	// private static boolean D = NeosaClientActivity.D;
	public static final String TAG = "Neosa";
	// Dialogos

	private static Dispositivo dispTemp = new Dispositivo();
	//private static int p = 0;
	Activity context;

	ArrayList<Dispositivo> devices_paired;

	DeviceAdapter(Activity context, ArrayList<Dispositivo> devices) {
		super(context, R.layout.main, devices);
		this.context = context;
		devices_paired = devices;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View item = inflater.inflate(R.layout.device_item, null);
		//p = position;
		final int po= position;
		TextView nombre = (TextView) item.findViewById(R.id.device_item_name);
		nombre.setText(devices_paired.get(position).getNombre());

		TextView mac = (TextView) item.findViewById(R.id.device_item_mac);
		mac.setText(devices_paired.get(position).getMac());
		// onSelectEvent
		item.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					
					dispTemp.setMac(devices_paired.get(po).getMac());
					dispTemp.setNombre(devices_paired.get(po).getNombre());
					PreferencesActivity.setDispositivo(dispTemp);
					context.showDialog(2);

				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});

		return item;
	}

}
